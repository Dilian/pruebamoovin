package rest;

import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LinearRing;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.geom.impl.CoordinateArraySequence;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

@Path("/Polygon")
public class PolygonTest {
    @POST
    @Path("/isPointInsidePolygon")
    @Consumes("application/x-www-form-urlencoded")
    @Produces(MediaType.APPLICATION_JSON)
    public Response isPointInsidePolygon(MultivaluedMap<String, String> formParams) {
        final GeometryFactory gf = new GeometryFactory();
        final ArrayList<Coordinate> points = new ArrayList();
        /*
         * En esta linea se agrego la ruta de un CSV con los datos proporcionados en el PDF de la prueba
         * */
        String csvFile = "C:\\Users\\Dilian\\Desktop\\Proyectos webaPP\\mavenproject1\\src\\main\\java\\rest\\Datos.csv";
        BufferedReader br = null;
        String line;
        String cvsSplitBy = ",";
        try {
            br = new BufferedReader(new FileReader(csvFile));
            /*
             *Recorre linea por linea el excel con los datos proporcionados para la prueba y obtiene la latitud(columna 2)
             * y longitud (columna 3) y lo guarda en un array de coordenadas llamado points
             * */
            while ((line = br.readLine()) != null) {
                String[] data = line.split(cvsSplitBy);
                points.add(new Coordinate(Double.parseDouble(data[1]), Double.parseDouble(data[2])));
                System.out.println(data[0] + ", " + data[1] + ", " + data[2]);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        Polygon polygon = gf.createPolygon(new LinearRing(new CoordinateArraySequence(points
                .toArray(new Coordinate[points.size()])), gf), null);

        /*Se prueba con los parametros recibidos en el POST si el punto pertenece aL poligono o no y
         * se guarda en un objeto de la clase response la cual es devuelta  como un JSON
         * */
        Response response = new Response();
        final Coordinate coord = new Coordinate(Double.parseDouble(formParams.getFirst("latitude")), Double.parseDouble(formParams.getFirst("longitude")));
        final Point point = gf.createPoint(coord);
        response.setInside(point.within(polygon));
        return response;
    }
}
