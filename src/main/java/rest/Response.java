package rest;

import javax.xml.bind.annotation.XmlElement;

public class Response {
    private boolean isInside;

    @XmlElement(name = "isInsidePolygon")
    public boolean isInside() {
        return isInside;
    }

    public void setInside(boolean inside) {
        isInside = inside;
    }
}
