package rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Dilian Badilla Mora
 */
@ApplicationPath("/")
public class RestApplication extends Application {
    private Set<Object> classes = new HashSet<Object>();

    public RestApplication() {
        classes.add(new PolygonTest());
    }

    @Override
    public Set<Object> getSingletons() {
        return classes;
    }

}